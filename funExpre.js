// Function Expression example.

var getSum = function(num1,num2){
    var total = num1+ num2;
    return total;
}

console.log(getSum(5,6)); // 11
var sum = getSum(7,8); // 15
console.log(sum);