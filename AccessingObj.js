// accessing Objects

var veg = {
    "veg1":"Potato",
    "veg2":"Cabbage",
    "veg3":"bitter",
    "veg4":"ladyfinger"
}
var vegetable1 = veg.veg1;
var vegetable2 = veg.veg2;
var vegetable3 = veg.veg3;
console.log(vegetable1);
console.log(vegetable2);
console.log(vegetable3);