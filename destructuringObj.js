// Destructuring Objects

var silverScreenDetails={name:"Chiranjeevi",tag:"super star"}
var realLifeDetails={realName:"Konidela siva prasad",tag:"Mega star"}
 
var allActors={...silverScreenDetails,...realLifeDetails}
console.log(allActors);

// Example 2
var a = {name:"swati", year:1990}
var b ={husbandName:"Amol",birthYear:1995}
var c ={...a,...b};
console.log(c);

// Example 3

var d = {rashi:"kumbh",tag:"divorcee"}
var f ={rashi:"mesh",tagNow:"Married"}
console.log({...d,...f});