//String Occurence.

var string = 'My name is Swati and My village is Kojbi',
searchFor = 'is',
count = 0,
positionOfStringIndex = string.indexOf(searchFor);

while (positionOfStringIndex  > -1) {
    ++count;
    positionOfStringIndex  = string.indexOf(searchFor, ++positionOfStringIndex );
}

console.log(count);   // 2