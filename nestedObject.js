// Nested Object accessing.

const person = {
    firstName:"John",
    lastName:"Doe",
    age:50,
    eyeColor:"blue",
    city:{
      city1:'USA',
      city2:'India',
      city3:'Dubai'
    }
  }
  
  console.log(person.city.city2);

  //Or

  console.log(person["city"]["city1"])

  // or

  console.log(person.city["city3"]);