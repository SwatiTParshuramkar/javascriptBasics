// Scope of Variable.

// var a =5;
// function func(){
//     var a =7;
// }
// console.log(a);

// Second example.

var b = 5;
function func(){
    // var b; // undefined
    console.log(b);
    b=6;
    }
  func()
  console.log(b); // 5